Responsible design
===================

*"Every label — whether for text, images, video, or a combination — comes with a set of assumptions that must be independently tested against clear goals and transparently communicated to users."*

We apply the methodology proposed in `this article`_ by First Draft. 

.. _this article: https://firstdraftnews.org/latest/it-matters-how-platforms-label-manipulated-media-here-are-12-principles-designers-should-follow/
