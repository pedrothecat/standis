Welcome to Standis's documentation!
===================================

STANDIS is a system and tool to address novel issues related to disinformation, trust and data integrity in the digital age. In specific, it is designed to:

* **Provide a framework to structure annotation of disinformation samples in compliance with already existent schema**

* **Popularize the structured annotation of disinformation samples among journalists, media organizations and experts**

* **Provide a decentralized system to store, verify and consult annotations of disinformation samples**

* **Provide a resilient framework to ensure and verify the integrity of disinformation annotations**

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   proof_concept
   integration_annotation
   integration_storage
   design
