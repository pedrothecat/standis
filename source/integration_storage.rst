Integration with external storage frameworks
============================================

Integration with BitTorrent
*****************************

For every annotation performed in Standis, a ``.torrent`` file is generated. This file contains instructions to download the annotated image.

**Standis servers automatically start to seed** this ``.png`` file and make it available to users' download.

This same file is **automatically uploaded to the IPFS network**.

Integration with IPFS
**********************

For every annotation performed in Standis, the following is uploaded to IPFS:

* A ``.json`` file containing the annotation metadata
* A ``.png`` file containing the annotated media
* A ``.torrent`` file containing the necessary metadata to download the ``.png`` file through the BitTorrent network

Integration with blockchain (Ethereum)
**************************************

Currently, users can choose whether to send or not their annotation to the Ethereum blockchain.

If the option is selected, the following fields will be stored in the *DRAFT (ERC-20 compliant) smartcontract*:

* ``ipfs_hash`` containing the IPFS cryptographic hash to download the annotation files
* ``standis_is`` containing the unique 8 hex characters identification string in the Standis system.

After that, a feeless *call* can be sent to the *DRAFT smartcontract*, providing the ``standis_is`` of an annotation and receiving its respective ``ipfs_hash`` value.

The feeless *getRatings call* provides listing of all the annotations stored in the *DRAFT smartcontract*.

**This function is currently working in Ethereum's Ropsten testnet.**

The DRAFT smart-contract
------------------------

The **Document of Reference for Annotation Format Token (DRAFT)** is an ERC-20 compliant smart-contract that uses the Ethereum blockchain framework. Its functions are:

* **Storage of annotations performed through Standis**

Trusted users/organizations can submit annotations to DRAFT smart-contract by spending 1 DRAFT. Once this is done, integrity and verifiability of its content/authors/timestamp is ensured by an extremelly resilient cryptographic framework.

* **Query of annotations performed through Standis**

Once an annotation is done, *anyone* can check its integrity - not depending on any central authority or infra-structure. For example, users can design *Standis explorers* if they want to. Querying for annotations is *feeless*.

* **A token to control and manage annotations performed through Standis**

Every annotations demands the spent of 1 DRAFT. By distributing them to selected members of the public and organizations, a network of trust is also built and Standis is able to manage who is able to perform annotations, when and why.

* **Provide the option to incentivize the maintenance of the Standis system**

DRAFT is also a token that in the future can be exchanged for any form of incentive in order to ensure the maintenance of Standis - both with users, external parties and members of the public. DRAFT can also be used as a way to allow selected entities to participate in decision-making processes related to the platform.

**Smart-contract details**

* Compliance: ERC20
* Decimals: none (to avoid unnecessary speculation)
* Supply: 10,000,000 tokens
* Circulation: distributed in limited batches to accredited organizations

**Contract functions**

How to use DRAFT
----------------

**How it works**

For every annotation performed through Standis sent to the blockchain, 1 DRAFT is spent.

Users using this function must have DRAFT in their Ethereum wallet as well as ETH in order to cover transaction (gas) costs.

Distribution of DRAFT to Standis users are performed on-demand. If your organization is willing to use it, contact support@stand.is .

**Step 1: install Metamask (browser plugin)**

Standis interface is integrated with DRAFT via the Metamask browser plugin. `Download it`_ and install.

.. _Download it: https://metamask.io/download.html

**Step 2: setup you wallet**

Create an account on Metamask and generate your first wallet. Instructions `here`_.

.. _here: https://medium.com/publicaio/a-complete-guide-to-using-metamask-updated-version-cd0d6f8c338f

**Step 3: request you DRAFTs**

Contact Standis by email (support@stand.is) or any other means and request your tokens. You will have to send your wallet address - you will have access to it via the Metamask interface.

After approval, DRAFTs will be deposited in your wallet and instructions to access it will be sent.

**Step 4: make sure you also have ETH in your account**

You will need ETH (the Ethereum main currency) in order to cover transaction costs when you send annotations to the blockchain. There are `many ways`_ in which you can acquire ETH. Standis staff might be able to provide you assistance in case you face troubles or have doubts.

Eventually, Standis will also be able to donate ETH to your organization - please state your request once you ask for DRAFTs.

.. _many ways: https://ethereum.org/en/get-eth/

**Step 5: perform your first annotation registered in the blockchain!**

Once you have DRAFT and ETH in your Metamask wallet, you will be able to send annotations to the blockchain by simply authorizing the transaction through the Metamask interface that will be automatically launched when you enable blockchain in Standis annotation interface!

.. note:: DRAFT is currently deployed in the Ropsten Ethereum develoment network and is now in testing phase

