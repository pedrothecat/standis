.. Standis documentation master file, created by
   sphinx-quickstart on Tue Feb  2 11:54:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Proof of concept
===================================

*"No statement is immune to revision" - V. O. Quine*

The magnitude of an information is in big part related to its capacity to influence systems. For instance, some information, or some sets of information, are able to shape society for centuries, millennia. "History of art", for example, can be considered as a set of well established information that continues to affect humans since Ancient times. This set of informational assumptions has been fed by thousands of men, from Herodotus to Hegel. For them, the fact that pyramids were constructed in Giza, and that they were erected by people called "Egyptians" thousands of years ago is not under dispute. But why? What seems to link this chain of thought is a chain of trust.

The fundamental element of trust still seems to define the capacity of information to affect systems, not only on a human level. Let's take the very contemporary situation when someone buys a good on the Internet using their credit card. When the card information is sent to a server of an online shop, this server sends the same information to banking servers. In this exchange, in order for the purchase to be completed the shop has to trust that the card is being used by a legitimate buyer, financial institutions must trust that the card number actually corresponds to an existent card and finally, to be short, that the amount necessary is available in the card owner's account. On a micro level, if this chain of trust is successful, the information contained in the credit card is able to affect a system; in this case by providing the buyer with a good and the seller with a compensation for that.

What sustains the chain of trust in both above-mentioned examples? An agreement on sources. Historians of art will trust in the account of historians they learnt to trust, or on whom they chose to trust. Internet users will trust in specific online stores, these stores will trust in specific financial providers and the latter will trust in specific analytics and infra-structure. Both on a machine and human level, there should be a "handshake" between two points that trust each other - and these points are not random, but very specific. Sources are entities upon which other entities deposit their trust in order to realize the potential of information to affect systems.

**Trust in the digital age**

In the digital age, trust is subjective, arbitrary and many times irrational. For some, a trustful source is someone from their familiar or friendship circles. To others, trust come from celebrities and social icons. Some tend to be more prone to believe in so called scientific sources. Some believe in some media, but not in others. What all this people with different parametters of trust have in common is that they allow information about something they haven't experienced, observed to join their spheres of belief - this is trust.

However, you don't need trust to believe in something you can experience yourself. You can doubt that the Sun is a gravitational force in the center of our solar system, but you can't doubt that a firy sphere rises everyday in the sky. To believe or not that the Earth is flat will depend on how much you trust others who allege to have experienced its roundness. But again, you can't disbelief that 2+2 equals 4, because your own experience proves the laws of sum everyday, in your own empirism. In other words, there are beliefs that are trustless, and these are the only universal ones.

The challenge of trust becomes specially problematic when we deal with information that is under clear, critical dispute. **We need a forensic verifiable, decentralized, self-empiric framework to build a system of annotation of (dis)information that can stand against any (un)reasonable attempts of distrust, tampering and misinterpretation - a system ultimately designed for skeptics.**

Trust in sources is constantly being revised, and we are always adapting our structure of belief to consider those whose statements are closer to our experience.

**Standing against distrust**

Standis allows *anyone* to re-make the steps of verification of the information input by journalists and experts in the system, in its most technical aspect. Cryptography-based networks allow verification of annotations without any human intermediation - you just need to believe in mathematics.

**Standing against tampering**

The consistency of the annotation (meta)data is again proved mathematically by cryptograpy-based frameworks. As such, *anyone* can verify the originality (or not) of a piece of annotation - timestamp, author, content. At the same time, any person or entity can also store and/or host all this public information without a centralized authority to testify for its alleged integrity.

**Standing against misinterpretation**

Expert design and narrative considerations allow Standis to be responsible and achieve high levels of neutrality and precision in the annotations, not inciting doubts or misplaced interpretations - even when the content is maliciously shared by third-parties.

The technical solution for trust in the digital age is not to improve systems of trust and the level of convincement of some sources, but it's about investing in systems that don't depend on trust and improve the agnosticism and solidity of sources. The current state of cryptography and decentralized networks allows us to start experimenting with that on a scalable way - very close to the public and at the same time connected with machines.

Welcome to Standis!
